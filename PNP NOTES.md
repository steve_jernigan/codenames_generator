Print and Play Notes
====================

Run 'bundle install' to install squib.  For more information on squib, see the
[squib webpage](http://squib.readthedocs.io/).

# Printing badges

Add words to the words.csv list as you like.  Then run
`ruby deck.rb`
to generate _output/badges.pdf.  

# Printing codebook cards

In key.rb, change the 'cards = 16' line to however many key cards you wish to
generate.  Then run
`ruby key.rb`
to generate the key to _output/codebook_sheet.pdf
