Simple squib-based generator for a card game.  I love Codenames but wanted to extend it to a 3 team game and try building something with squib.

[Rules](https://bitbucket.org/steve_jernigan/codenames_generator/src/1a13c194cf4fbdd86c1307d13351001c46395a21/RULES.md?at=master&fileviewer=file-view-default)

[PrintNPlay notes](https://bitbucket.org/steve_jernigan/codenames_generator/src/321ccfe42ec7a174d394306f2e42c5c1c720378b/PNP%20NOTES.md?at=master&fileviewer=file-view-default)