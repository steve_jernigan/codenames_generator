# Intro

SpyCon is the annual conference of international spies and it is in full swing in
your town. It just so happens that SpyCon is held in conjunction with
Cons4Rents, the perfectly innocent annual meeting of ex-convict, temporary
laborers.  Neither spies nor cons like being identified by their real name so
they have all written codenames or nicknames on their badges. The spies are so
secretive they do not even know who is on their side, who is their enemy, and who
is just an wrongly convicted felon.  Gather all your spies together first for a
secret breakout session and win the intelligence game.  

# Objective

Be the first team to give all your spies a ticket to your team's breakout
session and don't invite a foreign spy or wrongly convicted felon.

# Components

You have a deck of badges, several codebooks, a set of breakout session tickets
for each team.

# Gameplay

Divide players into 2 or 3 teams.  Each team will need a set of session tickets.
Shuffle the badges and layout 24 in a 6 by 4 grid.   Select one codebook but
keep it hidden. One player on each team will be the spy master.  The spy masters
will sit together so that they can all see the shared codebook.  The codebook
indicates which of the spies, represented by the badges, is on your side.  If
you are playing with only two teams, the third team is treated as wrongly
convicted felons.

Randomly decide a color for each team and a team to go first.  The spy master
for the team gives a single word clue that is not the name on any badge but
indicates a spy on their team.  They can also give a number if the clue pertains
to more than one spy.  The rest of the team then tries to identify the spies by
naming them one at a time.  If they identify a spy on their side, a breakout
session ticket is placed over the spy and they may guess again.  Guessing a spy
on another team ends the turn and the indicated spy is covered with a correctly
color breakout session ticket. Play immediately moves on to the next team if an
enemy spy or wrongly convicted felony is invited to your secret breakout
session. The guessing team can guess as many or as few times as they wish as
long as they don't make a mistake.

The first team to successfully cover their spies with breakout session tickets wins.

One more thing, each team has an assassin noted by a skull and crossbones on the
codebook.  If another team invites a foreign assassin to their breakout session,
they immediately loose the game.

# Credits

This game based on the original two team, Codenames game.  It is a great game and
a recommended purchase.
