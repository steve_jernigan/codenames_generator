require 'squib'
data = Squib.csv file: 'words.csv'
card_count =  data['words'].size

name_colors = %w(navy_blue dark_blue medium_blue blue prussian_blue midnight_blue dark_powder_blue smalt cobalt dark_slate_blue sapphire)
name_color_enum = name_colors.cycle

def myrand(value, dither)
  value - dither/2 + rand() * dither
end

Squib::Deck.new cards: card_count, layout: ['layout.yml'] do
  #background color: '(0,0)(400,0) lavender_pink@0.0 powder_blue@1.0'
  deck = csv file: 'words_test.csv'
  rect layout: 'bottom_background'
  rect layout: 'top_background'

  rect layout: 'cut' # cut line as defined by TheGameCrafter
  #rect layout: 'safe' # safe zone as defined by TheGameCrafter

  text str: "HELLO", layout: 'hello_str'
  text str: "my name is", layout: 'my_name_is_str'

  text str: data['words'].collect{|x| x.capitalize}, layout: 'name_str',
    color: name_color_enum.next, angle: myrand(1.571,0.05), x: myrand(500,20),
    y: myrand(37.5, 40)
  text str: data['words'].collect{|x| x.capitalize}, layout: 'upside_down_name_str'
  #save_png prefix: 'hello_'
  save_pdf trim: 37.5, file: 'badges.pdf'
  #save_sheet columns: 4, rows: 2, prefix: 'sheet'
end
