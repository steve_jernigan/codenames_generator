require 'squib'

cards = 16

#lighter to darker gradient for each team
team_colors = %w(azure medium_blue light_sea_green sea_green carnation cardinal)

columns = 4
rows = 6
startx = 75
starty = 75
width =  675
height = 975
gapx = 45
gapy = 25
badgex = (width - (columns +1) * gapx)/columns
badgey = (height - (rows +1) * gapy)/rows

#returns [[team,assassin],...]
def createTeams(n,teams =3)
  raise "#{n} positions does not divide evenly into #{teams} teams" if n%teams != 0
  a = (0..(n-1)).to_a.shuffle
  result = []
  n.times {result << [0,0]}
  team_count = 0
  a.each do |pos|
    result[pos] = [team_count % teams, team_count < teams]
    team_count +=1
  end
  result
end

#assumes a rectangular array
def pivot_table(arr)
  result = Array.new(arr[0].size){[]}
  arr.each_with_index do |a,i|
    a.each_with_index do |v,j|
      result[j][i] = v
    end
  end
  result
end

teams =  Array.new(cards){ createTeams(columns*rows)}
assassin_strs = pivot_table(teams.collect{ |x| x.collect {|x,y| y ? "N" : nil}})
teams_ids = pivot_table(teams.collect{ |x| x.collect {|x,y| x}})

Squib::Deck.new cards: cards, layout: ['layout.yml'] do

  posy = 75
  rows.times do |r|
    posy += gapy
    posx = 75
    columns.times do |c|
      pos = c + columns * r
      posx += gapx
      t = teams[0][pos]
      #puts "#{pos} #{t}"
      rect layout: 'code_badge', x: posx, y: posy, width: badgex, height: badgey,
       fill_color: teams_ids[pos].collect{ |t| "(#{posx},#{posy})(#{posx+badgex},#{posy+badgey}) #{team_colors[t * 2]}@0.0 #{team_colors[1 + t * 2]}@1.0"},
       stroke_color: assassin_strs[pos].collect {|x| x == 'N' ?  "yellow" :'black'},
       stroke_width: assassin_strs[pos].collect {|x| x == 'N' ? 3 : 1}
      text str: assassin_strs[pos], layout: 'assassin_h_str', x: posx + badgex, y: posy, width: badgey, height: badgex
      posx += badgex
    end
    posy += badgey
  end

  rect layout: 'cut' # cut line as defined by TheGameCrafter
  #rect layout: 'safe' # safe zone as defined by TheGameCrafter

  #save_png prefix: 'codebook_'
  save_pdf trim: 37.5, file: 'codebook_sheet.pdf'
  #save_sheet columns: 4, rows: 2, prefix: 'codebook_sheet'
end
